# -*- coding: utf-8 -*-
# Gaze contingent presentation based on psychopy demo script
# Nat Melnik
# C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\hw\sr_research\eyelink\eyetracker.py
# supported dev: C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\supported_config_settings.yaml
from psychopy import visual, core, monitors,event
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
import os
import time
import numpy
import show_paragraph
import util

use_eyelink=True

target_location=(0,-200) # Center (pixels) of letter or trigram to identify
target_flanked=True
target_mocs_heights=[30,50,80,100]
mocs_trials_per=10

fixation_size=12

thresh_pixels=9999
scotoma_radius=120
#pos_scotoma=[0,-scotoma_radius*1.2] #IVF
pos_scotoma=[-scotoma_radius*1+50,0] #LVF
pos_scotoma=[ scotoma_radius*1+50,0] #RVF

try:
    os.remove('et_data.EDF')#nm: remove edf file from the previous trial... in ideal case should save directly with another name... have not figured out yet.
except OSError:
    pass

class ExperimentRuntime(ioHubExperimentRuntime):
    def run(self,*args):

        if use_eyelink:
            display=self.hub.devices.display
            kb=self.hub.devices.keyboard
            mouse=self.hub.devices.mouse
            tracker=self.hub.devices.tracker

            res=display.getPixelResolution() # Current pixel resolution of the Display to be used

        monitorsetting = monitors.Monitor('default', width=39.5, distance=57)

        if use_eyelink:
            tracker.runSetupProcedure()

        monsize =  [1024, 768]
        monitorsetting.setSizePix(monsize)
        window=visual.Window(monsize, units='pix', fullscr=False,allowGUI=True,color=[0,0.0,0], screen=display.getIndex())

        fixation_horizontal=visual.Line(win=win,start=(-fixation_size//2,0), end=(fixation_size//2,0), lineColor=[-1,-1,-1], lineWidth=2 )
        fixation_vertical=visual.Line(win=win,start=(0,-fixation_size//2), end=(0,fixation_size//2), lineColor=[-1,-1,-1], lineWidth=2 )

        target = visual.TextStim(myWin,pos=letter_location,alignHoriz='center', alignVert='center', text="Target")

        window.recordFrameIntervals=False

        mouse.setSystemCursorVisibility(False)
        clock = core.Clock()

        gaze = visual.Circle(win=window, radius=scotoma_radius, pos=(0, 0), lineWidth=0,
                                  edges=20, fillColor=[0,0,0], units='pix')

        self.hub.clearEvents('all')
        tracker.setRecordingState(False)

        expt=show_paragraph.reading_expt(0)
        expt.setup(win=window)
        expt.draw_setup() 
        expt.draw1() # Renders the text to the back buffer
        window.color=[0,0,0]
        shot=visual.BufferImageStim(window)
        
        show_gaze=True
        done=False

        highlighted=0
        highlighted_row=0
        window.recordFrameIntervals=True
        while done==False:
            clock.reset()
            self.hub.clearEvents('all')
            tracker.setRecordingState(True)

            keys=event.getKeys()
            if 'g' in keys:
                show_gaze=not (show_gaze)

            if 'up' in keys:
                highlighted_row = (highlighted_row - 1) % (len(expt.row_pos))
                shot.mask=expt.apply_line_mask(highlighted_row)
            elif 'down' in keys:
                highlighted_row = (highlighted_row + 1) % (len(expt.row_pos))
                shot.mask=expt.apply_line_mask(highlighted_row)

            if rerender:
                window.setRecordFrameIntervals(False)
                expt.draw_setup()
                window.setRecordFrameIntervals(False)
                redraw=True
                highlighted=0 # to prevent problems 

            if redraw:
                window.setRecordFrameIntervals(False)
                expt.draw1()
                shot=visual.BufferImageStim(window)
                window.setRecordFrameIntervals(False)                
                
            gpos=tracker.getPosition()
            gaze_ok=False # only draw if gaze is detected and near the fovea
            show_gaze_menu=False # default is not in menu
            
            if type(gpos) in [tuple,list]:
                gaze.pos = gpos
                # Find the word directly below the scotoma
                #highlighted=expt.find_spotlight(gpos[0]+pos_scotoma[0],gpos[1]+pos_scotoma[1])
                # Use the eye X position and the (manually incremented) highlighted_row
                highlighted=expt.find_spotlight(gpos[0]+pos_scotoma[0],row=highlighted_row)
            
            shot.draw()
            
            expt.stims_words[highlighted].color=[1,1,0]
            expt.stims_words[highlighted].draw()
            
            if show_gaze: # GC scotoma--always show for us
                gaze.draw()
                
            flip_time=window.flip()

        # After the main loops exits:
        window.recordFrameIntervals=False
        
        tracker.setRecordingState(False)
        tracker.setConnectionState(False)

        self.hub.quit()
        
        #print os.getcwd()
        #nm: move edf file created during the trial and rename it with trial's name.
        #if not os.path.exists(os.getcwd() +'/'+'eye_track_data'):
        #    os.makedirs(os.getcwd() +'/'+'eye_track_data')
        #newname = os.getcwd() +'/'+'eye_track_data'+expt.outfilename[7:-3]+'EDF'
        #os.rename('et_data.EDF', newname) #nm: renames/copies the file to the folder
        #print "EDF file was saved in ",newname

        #expt.finish()
        
        #import matplotlib.pyplot as plt
        #plt.plot( window.frameIntervals)
        #plt.show()
        window.saveFrameIntervals()
        window.close()
        core.quit()
        
runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
runtime.start()


