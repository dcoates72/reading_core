# -*- coding: utf-8 -*-
# Gaze contingent presentation based on psychopy demo script
# Nat Melnik
# C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\hw\sr_research\eyelink\eyetracker.py
# supported dev: C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\supported_config_settings.yaml
from psychopy import visual, core, monitors,event
import os
import numpy
import time
import util
import show_paragraph
import numpy as np

subject_name='kr'
condition='span'

use_eyelink=False

target_location=(0,-200) # Center (pixels) of letter or trigram to identify
target_flanked=True
target_mocs_heights=[30,50,80,100]
eccs=np.linspace(-200,200,9)
mocs_trials_per=20

fixation_size=10

thresh_pixels=9999
scotoma_radius=120

eccs_sequence=np.random.permutation( range(len(eccs))*mocs_trials_per )
#heights_sequence=np.random.permutation( range(len(target_mocs_heights))*mocs_trials_per )

total_trials=len(eccs_sequence)

# Which index into the targeT_mocs_heights array will be used for all trials? (0th)
heights_sequence=[0]*( total_trials )


trialnum=0
if True: # eventually put into if _main_
        if use_eyelink:
            display=self.hub.devices.display
            kb=self.hub.devices.keyboard
            mouse=self.hub.devices.mouse
            tracker=self.hub.devices.tracker

            res=display.getPixelResolution() # Current pixel resolution of the Display to be used

        monitorsetting = monitors.Monitor('default', width=55, distance=57)

        if use_eyelink:
            tracker.runSetupProcedure()

        monsize =  [1024, 768]
        monitorsetting.setSizePix(monsize)
        window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0.0,0], screen=1)

        fixation_horizontal=visual.Line(win=window,start=(-fixation_size//2,0), end=(fixation_size//2,0), lineColor=[-1,-1,-1], lineWidth=2 )
        fixation_vertical=visual.Line(win=window,start=(0,-fixation_size//2), end=(0,fixation_size//2), lineColor=[-1,-1,-1], lineWidth=2 )

        target = visual.TextStim(window,pos=target_location,alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] )

        window.recordFrameIntervals=False

        #mouse.setSystemCursorVisibility(False)
        clock = core.Clock()

        gaze = visual.Circle(win=window, radius=scotoma_radius, pos=(0, 0), lineWidth=0,
                                  edges=20, fillColor=[0,0,0], units='pix')

        window.color=[0,0,0]
        
        show_gaze=False
        done=False

        fixation_horizontal.draw()
        fixation_vertical.draw()
        flip_time=window.flip()
        window.recordFrameIntervals=True

        outfilename = util.get_unique_filename("results/%s_%s_%s-%%02d.csv" % (subject_name, condition,
                    time.strftime("%m%d%Y", time.localtime() ) ) )
        outfile = open(outfilename, "wt")
        outfile.write("%s,%s,%s,%s,%s,%s,%s\n"%("resp","left","target","right","size","ecc","corr") )

        keys=event.waitKeys()

        total_trials=len(heights_sequence)
        trial_num=0
        while trial_num<total_trials:
            clock.reset()

            if 'escape' in keys:
                break;

            target.height=target_mocs_heights[heights_sequence[trial_num]]
            target.pos=[eccs[eccs_sequence[trial_num]],0]
            if target_flanked==True:
                trigram=''.join([chr(np.random.randint(26)+ord('a')) for n in [0,1,2] ])
                target_let=trigram[1]
            else:
                trigram=''.join([chr(np.random.randint(26)+ord('a')) for n in [0] ])
                target_let=trigram[0]
            target.text=trigram
            target.draw()
            
            if show_gaze: # GC scotoma--always show for us
                gaze.draw()
                
            if target.pos[0] != 0:
                fixation_horizontal.draw()
                fixation_vertical.draw()

            flip_time=window.flip()
            core.wait(0.15) # not the best way to ensure timing, but...

            # Then, redraw fixation and wait for any key
            fixation_horizontal.draw()
            fixation_vertical.draw()
            flip_time=window.flip()

            trial_num += 1
            keys=event.waitKeys()

            if keys[0]==target_let:
                corr=1
            else:
                corr=0

            # Write out their response and the input string to the file
            if target_flanked:
                outfile.write("%s,%s,%s,%s,%d,%d,%d\n"%(keys[0],trigram[0],trigram[1],trigram[2],target.height,
                    target.pos[0], corr) )
            else:
                outfile.write("%s,%s,%s,%s,%d,%d,%d\n"%(keys[0],"",trigram[0],"",target.height,target.pos[0],corr) )
        outfile.close()
        # After the main loops exits:
        window.recordFrameIntervals=False
        
        window.saveFrameIntervals()
        window.close()
        core.quit()
