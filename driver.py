from simulated import ExperimentRuntime
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
import os 
#import question_display #The question display function is needed for when the comprehension questions are needed to be asked
import numpy as np
from psychopy import gui

#Changes to the path to the folder which contains the subject protocol
file_location=os.path.join(os.getcwd(),'Randomization_Protocol')
os.chdir(file_location)

#Creates a GUI where the examiner enters the information of the subject that is to be tested
myDlg = gui.Dlg(title="Reading Experiment")
myDlg.addText('Subject Information')
myDlg.addField('Name:')
myDlg.addField('Subject ID:',1)
myDlg.addText('Experiment Info: Number Presentation')
myDlg.addField('Session Number:',1)
myDlg.addField('Training Required:', choices=["False", "True"])
myDlg.addField('Foveating Saccade Detector: Sensitivity',choices=["Off","Low", "High"])
ok_data = myDlg.show()  # show dialog and wait for OK or Cancel
if myDlg.OK:  # or if ok_data is not None
    subj_id=ok_data[1]
    session_num=ok_data[2]
    if ok_data[3] == "False":
        training="False"
    elif ok_data=="True":
        training="True"
    if ok_data[4]=="off":
        foveation_tracker="off"
    elif ok_data[4]=="Low":
        foveation_tracker="low"
    elif ok_data[4]=="High":
        foveation_tracker="high"
else:
    print('user cancelled')


if subj_id==1:
    if session_num==1:
        subj_file=open("S01_01.txt","rt")
    else:
        subj_file=open("S01_02.txt","rt")
if subj_id==2:
    if session_num==1:
        subj_file=open("S02_01.txt","rt")
    elif session_num==2:
        subj_file=open("S02_02.txt","rt")

if subj_id==3:
    if session_num==1:
        subj_file=open("S03_01.txt","rt")
    elif session_num==2:
        subj_file=open("S03_02.txt","rt")

if subj_id==4:
    if session_num==1:
        subj_file=open("S04_01.txt","rt")
    else:
        subj_file=open("S04_02.txt","rt")
        
if subj_id==5:
    if session_num==1:
        subj_file=open("S05_01.txt","rt")
    else:
        subj_file=open("S05_02.txt","rt")
        
if subj_id==6:
    if session_num==1:
        subj_file=open("S06_01.txt","rt")
    else:
        subj_file=open("S06_02.txt","rt")
    
    
#trial_file=subj_file.readlines() # pull this from first line of file
trial_lines=subj_file.readlines()
subj_id=trial_lines[0].strip()
training_file=open("learning_package.txt","rt")
training_lines=training_file.readlines()
os.chdir("..")

if training=="True":
    for ntrial in range(len(training_lines)):
        runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
        runtime.subject=subj_id # pull this from file
        learning_fields=training_lines[ntrial].split(",")
        learning_para=int(learning_fields[1])
        
        # Run the eye-tracking experiment:
        runtime.subject=subj_id
        runtime.trial_num=0
        runtime.session_num=0
        runtime.rand_seed=(learning_fields[2])
        runtime.paragraph_number=learning_para
        runtime.eye_mode=learning_fields[0]
        runtime.fixation_cross=True
        runtime.foveation_sensitivity=foveation_tracker

        runtime.start()
        
        
        # Ask the 3 behavioral questions, which returns the number of correct answer
        #results=question_display.ask3questions(learning_para)
        
else:
    #filename='results_%s_%d.txt'%(subj_id,session_num)

    #results_file=open(filename, "at")
    for ntrial in np.arange(len(trial_lines)-1):    
        runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
        runtime.subject=subj_id # pull this from file
        trial_fields=trial_lines[ntrial+1].split(",")
        paragraph_number=int(trial_fields[1])
    
        # Run the eye-tracking experiment:
        runtime.subject=subj_id
        runtime.trial_num=ntrial
        runtime.rand_seed=int(trial_fields[2])
        runtime.session_num=session_num
        runtime.paragraph_number=paragraph_number
        runtime.eye_mode=trial_fields[0]
        runtime.fixation_cross=False
        runtime.foveation_sensitivity=foveation_tracker

        runtime.start()
        
        #print trial_fields[0],trial_fields[1]
    
        # Ask the 3 behavioral questions, which returns the number of correct answer
        #results=question_display.ask3questions(paragraph_number)
        # Returns a tuple like (0,1,1) for last two Qs correct
        #results_file.write(str(results)+"\n")
        
    #results_file.close()
        
