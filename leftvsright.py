import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core

import sys
import numpy as np
import time
import pdb
import random
import pandas as pd


debug_draw_multiline=False # whether to draw a 'normal' multiline Text box

def get_paragraph(which_paragraph,max_paragraphs=10):
    # TODO: Does this need to come from a different paragraph?
    answer_file=pd.read_csv("./answer_file_filter.csv",delimiter='\t',header=None)
    answer_array=np.array(answer_file)
    idx=np.argwhere( answer_array[:,0]==which_paragraph ).squeeze()
    para=answer_array[idx][1]
    return para

def zero_search(arr,startat):
# startat allows skip for for startat letters
    idx=np.max( np.arange(len(arr[startat:]))[arr[startat:]>0] )+startat
    return idx

def process_win(win,startat=0):
    buf2=win._getFrame("back")
    bw=np.mean(np.array(buf2),2) # convert to BW by taking the mean across the color channels
    #bw=np.array(buf2) # convert to BW by taking the 0th color channels
    vertical_sum=np.sum(bw,0)    # vertical sum (across cols)
    vertical_sum = vertical_sum - np.min(vertical_sum) # normalize to min of zero
    
    vertical_sum = vertical_sum / np.max(vertical_sum) # normalize to max of 1
    #vertical_sum = vertical_sum * height #  only important when plotting to see cross section

    return bw,vertical_sum,zero_search(vertical_sum,startat)

def get_xlocs(win, nwords, words, text, showProgress=True):
    xlocs=np.zeros(nwords)

    win.clearBuffer()
    text.setText("X ")
    text.draw()
    bw,vertical_sum,end_X=process_win(win)
    
    prog_bar = visual.Rect(win=win, pos=(0,0), size=(200,30), lineColor='black',fillColor='black')

    for nword in np.arange(nwords):
        win.clearBuffer()
        txt="%s X"%words[nword]
        text.setText(txt)
        text.draw()
        
        bw,vertical_sum,word_end=process_win(win)
        xlocs[nword]=word_end-end_X
        
        if showProgress:
#            text.setText("%d/%d"%(nword,nwords) )
            win.clearBuffer()
            prog_bar.size=(200*(1.0-float(nword)/nwords),30)
            prog_bar.draw()
            win.flip()

    return xlocs,end_X

def calc_and_draw(nwords,stims_words,xlocs,end_X,pos_left,width_window,height,line_spacing,max_lines=5):
    posx=pos_left[0]
    posy=pos_left[1]

    nlines=1
    nrow=0
    npage=0

    for nword in np.arange(nwords):
        stims_words[nword].len=xlocs[nword]-end_X

        # Does the next word go to the next line?
        if (posx+xlocs[nword]-end_X) >= width_window/2.0:
            stims_words[nword-1].eol=True

            # Hit the maximum # of lines/page?
            if nlines>=max_lines:
                if True: # one page only
                    return nword # don't include this word (on second page)
                else: # multiple pages
                    nlines=1
                    npage += 1
                    posy=pos_left[1]
            else:
                posy -= height*line_spacing
                nlines += 1

            posx=pos_left[0]
            
        stims_words[nword].eol=False
        stims_words[nword].nrow=nrow
        stims_words[nword].npage=npage
        stims_words[nword].pos=[posx,posy]
        stims_words[nword].draw()

        posx += xlocs[nword]
    return nword+1
        
def load_paragraph(which_paragraph,myWin,text,pos_left,height,color_text_draw,king_devick_mode):

    if type(which_paragraph) is str:
        sentence=which_paragraph
        king_devick_mode=False # Don't re-randomize King-Devick: use as-is
    else:
        sentence=get_paragraph(which_paragraph)

    words=sentence.split(" ")
    nwords=len(words)
    #print type(which_paragraph), words[0]

    if king_devick_mode:
        # King-Devick Mode
        fillchar="?" 
        for nword,word in enumerate(words):
            num1=np.random.randint(0,10)
            if fillchar=="=":
                lets=word[1:]
            elif fillchar=="?":
                #randlets=np.random.randint(0,27,ipytho)
                lets=''.join([chr(c+ord('a')) for c in np.random.randint(0,26,len(word)-1)])
            else:
                lets=fillchar*(len(word)-1)

            if False:
                # Make first character number
                words[nword]="%d%s"%(num1,lets)
            else:
                # Make second character number
                if len(lets)>1:
                    words[nword]="%s%d%s"%(lets[0],num1,lets[1:])
                else:
                    words[nword]="%s%d"%(lets,num1) 


    xlocs,end_X=get_xlocs(myWin, nwords, words, text)

    # Positions are filled in later, word-by-word
    stims_words = [visual.TextStim(myWin,pos=(0,0),alignHoriz='left', alignVert='center',
                                   height=height, text=words[nword],
                                   color=color_text_draw) for nword in np.arange(nwords) ]

    return nwords,words,stims_words,xlocs,end_X

# DRC: this does some drawing that it shouldnt
def draw_setup(which_paragraph,myWin,text,pos_left,width_window,height,color_text_draw,line_spacing,max_lines,king_devick_mode):
    myWin.clearBuffer() 
    #myWin.color=color_back_calc
    myWin.flip()

    nwords,words,stims_words,xlocs,end_X=load_paragraph(which_paragraph,myWin,text,pos_left,height,color_text_draw,king_devick_mode=king_devick_mode)

    myWin.clearBuffer() 
    #myWin.color=color_back_draw
    myWin.flip()
    # See http://www.psychopy.org/api/visual/window.html://www.psychopy.org/api/visual/window.html -- need two flips to change BG color

    if debug_draw_multiline:
        # If want to draw the multiline TextStim:
        text.setText(" ".join(words))
        text.color=(0,1,0)
        text.pos=pos_left # It was left-justified to 0 for calculations
        text.draw()

        text.pos[0]=-width_display/2.0 # Reset this lest we forget...

    newlen=calc_and_draw(nwords,stims_words,xlocs,end_X,pos_left,width_window,height,line_spacing,max_lines)
    #nwords=len(stims_words) # update, since this might have changed based on # of lines to show)

    myWin.clearBuffer()
    return stims_words[0:newlen],words

class reading_expt():
    def __init__(self,which_paragraph=0,max_lines=-1):
        # Feature control:
        self.which_paragraph=which_paragraph
        self.max_lines=max_lines
        self.apply_gaussian=True
        self.apply_highlight=True
        self.newpage_fixation_screen=True

        # Display control:
        self.line_spacing=1.50
        self.height=34
        self.width=(self.height*0.52)#dividing based on the aspect ratio for arial font
        self.top_window=200
        self.width_window=800
        self.height_window=600
        self.window_pos=np.array([-self.width_window/2.0,self.top_window/8.0]) # We divide the y position by 8 so as to center the text to the center of the window
        self.color_back_calc=(0,0,0)
        self.color_back_draw=(0,0,0)
        self.color_text_calc=(1,1,1)
        self.color_text_draw=(-1,-1,-1)
        self.color_highlight=(1,1,-1)
        self.color_fixation=(0.75,0.75,0.75)
        self.fix_lw=2
        self.fix_radius=10 # size of the flanges of the sword
        self.mask_sigma=60

        # Inits:
        self.npage=0 # which page are we currently displaying
        self.which_line=0 # which line is currently "highlighted" (& gaussianed)
        self.highlighted=0

        return
        
    def setup(self,win):
        ''' This should only be called once, when we first get the window '''
        self.win=win
        self.width_display,self.height_display=win.size
        self.pos_left=np.array([-self.width_window/2.0,self.height_window/2.0])

        # Fixation "sword"
        self.fixation_h=visual.Line(win=self.win, lineWidth=self.fix_lw, lineColor=self.color_fixation, units='pix')
        self.fixation_v=visual.Line(win=self.win, lineWidth=self.fix_lw, lineColor=self.color_fixation, units='pix')
        #self.fixation_v2=visual.Line(win=self.win, lineWidth=fix_lw, lineColor=fixation_color, units='pix')

        # For successive pages:
        self.fixation = visual.TextStim(self.win,alignHoriz='center', alignVert='center',text='+', color=[-1,-1,-1],height=30)
        return

    def recompute_positions(self):
        ''' Store information about positions for easy/fast determination of spotlight location
            so find_spotlight() can be called in draw loop
        '''
        self.row_pos=np.array([[self.stims_words[0].pos[1],0,0]])
        self.rows={}
        for nstim,astim in enumerate(self.stims_words):
            if astim.pos[1] != self.row_pos[-1][0]:
                self.row_pos[-1][2]=nstim-1
                self.row_pos=np.concatenate( (self.row_pos,[[astim.pos[1],nstim,0]]) )
        self.row_pos[-1][2]=nstim

        for nrow,arow in enumerate(self.row_pos):
            self.rows[nrow]=[self.stims_words[nstim].pos[0] for nstim in np.arange(arow[1],arow[2]+1,dtype='int')]
        
        
    def draw_setup(self):
        ''' Call this whenever big parts of stimulus change (window size, font size, etc. '''
       
        self.text = visual.TextStim(self.win,pos=(-self.width_display/2,self.height_window/2.0),
                alignHoriz='left', alignVert='center',
                height=self.height, color=self.color_text_calc,text="X")
        self.text.wrapWidth=self.width_window
        self.stims_words,self.words=draw_setup(self.which_paragraph,self.win,self.text,self.window_pos,self.width_window,self.height,self.color_text_draw,self.line_spacing,self.max_lines,king_devick_mode=self.king_devick_mode)
        self.recompute_positions()
        self.maxpage=self.stims_words[-1].npage
        
    def draw1(self,npage=0):
        for astim in self.stims_words:
            if astim.npage==npage:
                astim.color=self.color_text_draw
                astim.draw() 

    def find_spotlight(self,x,y=None,row=None):
        if row is None:
            row=int(round((self.row_pos[0][0]-y)/(self.line_spacing*self.height) ) ) 

        row += self.max_lines*self.npage # offset to get to correct page

        try:
            cols=self.rows[row]
        except KeyError: # It might exceed the last one
            if row<0:
                row=0
            else:
                row=len(self.row_pos)-1
            cols=self.rows[row]

        matches=np.where(np.array(cols)<x)
        try:
            match=matches[0][-1]+self.row_pos[row][1]
        except IndexError: # if to the left, use the first entry
            match=self.row_pos[row][1]

        return int(match) #,matches,cols,row

    def adjust_pos(self,x,y):
        ''' adjust the window position. Doesn't require rerendering, since everything just shifts.'''
        pos_shift=np.array([x,y])

        for astim in self.stims_words:
            astim.pos += pos_shift

        for nrow,arow in enumerate(self.row_pos):
            arow[0] += pos_shift[1] # shift the y location of each row
            self.rows[nrow] + pos_shift[0] # shift the x location of each row

    def adjust_line_spacing(self,multiple):
        ''' adjust the line spacing in multiples of height. Doesn't need to re-render, just shift lines. '''
        self.line_spacing=multiple
        for nrow,arow in enumerate(self.row_pos):
            arow[0] = self.window_pos[1]-nrow*self.height*self.line_spacing  # Compute the row y pos
            for nstim in np.arange(arow[1],arow[2]+1,dtype='int'):
                self.stims_words[nstim].pos = (self.stims_words[nstim].pos[0],arow[0])
                self.stims_words[nstim].color=[1,0,0]

    def apply_line_mask(self,which_line):
        yval=self.row_pos[which_line][0]
        yrange=np.linspace( 768/2.0,-768/2.0,768)
        sig=self.mask_sigma # fudge: how big the Gaussian for the line mask
        mask1=-1.0+2.00*np.exp(-(yrange-yval)**2/sig**2)
        mask=np.tile(mask1.T, (1024,1) ).T
        mask=np.array(np.reshape(np.flipud(mask),(1024,768) ) )
        return mask

    def make_mask(self, mask_radius, num_lets=12):
        # Erase what is already on screen:
        self.win.clearBuffer()

        # Make a bunch of letter widgets...
        lets=[visual.TextStim(self.win,alignHoriz='center',alignVert='center',height=self.height,color=self.color_text_draw)
            for alet in np.arange(num_lets)]
    
        # Then randomize the text, position, and rotation of those widgets, also draw each:
        for nlet,alet in enumerate(lets):
            alet.text=chr(ord('a')+np.random.randint(0,26))
            alet.ori=np.random.randint(-135,-45)
            alet.pos=(-60+(nlet//10)*15,-60+(nlet%10)*15)
            alet.draw()
    
        # Save the image of the scrambled letters:
        scrambled_letters=visual.BufferImageStim(self.win)

        # Extract the pixels of the scrambled image into an image widget
        scrambled_pixels=(np.array(scrambled_letters.image)[(self.height_display-mask_radius)//2:(self.height_display+mask_radius)//2,
            (self.width_display-mask_radius)//2:(self.width_display+mask_radius)//2,0]/128.0)-1.0
        # /128 goes from uint8 to float, -1.0 makes it relative to gray background

        # Now create the ImageStim to draw an populate with scrambled
        scotoma = visual.ImageStim(win=self.win, size=(mask_radius,mask_radius),pos=(0, 0), mask='raisedCos') #, maskParams={'fringeWidth':0.2})
        scotoma.image=scrambled_pixels
        self.win.clearBuffer()
        return scotoma

    def make_background(self):
        ''' Save the background as a buffer image we use for fast drawing.
            contains the text as black on white. Highlight is drawn over top of this.
            Draw the image once to see it.
        '''
        self.draw1(self.npage)
        self.background=visual.BufferImageStim(self.win)
        if self.apply_gaussian:
            self.background.mask=self.apply_line_mask(self.which_line)
        self.win.clearBuffer()

    def show_highlight(self,row=None):
        if row==None:
            row=self.which_line

        if self.scotoma_prl==270:
            target_word_x=self.eye_pos[0]
        elif self.scotoma_prl==0:
            target_word_x=self.eye_pos[0]+(self.scotoma_radius//2+(self.width*2))
        elif self.scotoma_prl==180:
            target_word_x=self.eye_pos[0]-(self.scotoma_radius//2+(self.width*2))


        self.highlighted=self.find_spotlight(target_word_x,row=row)
        if self.apply_highlight:
            self.stims_words[self.highlighted].color=self.color_highlight
            self.stims_words[self.highlighted].draw()
        #expt.stims_words[highlighted].text=spaceout(expt.stims_words[highlighted].text) # TODO: spaceout

    def show_fixation_sword(self):
        #if clauses for left to right condition
        if self.scotoma_prl==270 and self.direction=='left':
            offset=[0,self.scotoma_radius//2+self.height//2]
        elif self.scotoma_prl==0 and self.direction=='left':
            offset=[-(self.scotoma_radius//2+self.width*2),0]#To get the PRL to the center of the second charecter
        elif self.scotoma_prl==180 and self.direction=='left':
            offset=[+(self.scotoma_radius//2+self.width*2),0]#To get the PRL to the center of the second charecter

        #if clauses for right to left condition
        elif self.scotoma_prl==270 and self.direction=='right':
            offset=[768,self.scotoma_radius//2+self.height//2] # The fixation cross is moved to the right of the display
        elif self.scotoma_prl==0 and self.direction=='right':
            offset=[768+(self.scotoma_radius//2+self.width*2),0]# The fixation cross is moved to the right of the display
        elif self.scotoma_prl==180 and self.direction=='right':
            offset=[768-(self.scotoma_radius//2+self.width*2),0]# The fixation cross is moved to the right of the display

        if self.direction=='left':
            fix_center=[self.stims_words[self.highlighted].pos[0]+offset[0],
            self.stims_words[self.highlighted].pos[1]+offset[1]]
        elif self.direction=='right':
            fix_center=[self.stims_words[self.highlighted].pos[0]+offset[0],
            self.stims_words[self.highlighted].pos[1]+offset[1]]


        word_len=self.stims_words[self.highlighted].len

        self.fixation_h.vertices=[[fix_center[0]-self.fix_radius,fix_center[1]],
            [fix_center[0]+self.fix_radius,fix_center[1]]]
        self.fixation_v.vertices=[[fix_center[0],fix_center[1]-self.fix_radius],
            [fix_center[0],fix_center[1]+self.fix_radius] ]
        #self.fixation_v2.vertices=[[fix_center[0]+word_len,fix_center[1]-self.fix_radius],[fix_center[0]+word_len,fix_center[1]+fix_radius]]

        self.fixation_h.draw()
        self.fixation_v.draw()

    def line_dec(self):
        self.which_line -= 1 #= (which_line - 1) % (len(expt.row_pos))
        if self.which_line<=0:
            self.which_line=0
        if self.apply_gaussian:
            self.background.mask=self.apply_line_mask(self.which_line)

    def line_inc(self):
        ''' Advance to next line. Returns True if last line on page'''
        self.which_line += 1
        if self.apply_gaussian:
            self.background.mask=self.apply_line_mask(self.which_line)
        if self.which_line > self.max_lines-1:
            self.which_line=self.max_lines-1
            return True
        return False
                
    def show_newpage_fixation(self,wait_keys=True):
        self.win.clearBuffer()
        self.show_fixation_sword()
        self.win.flip()
        if wait_keys:
            keys=event.waitKeys() 

    def process_keys(self,keys,tracker=None):
        redraw=False
        rerender=False
        quit=False
        expt=self # legacy, since originally keyloop outside class. Could chg all to use "self"
        extra_keys=[]
        for akey in keys:
            if 'escape' in keys:
                quit=True
            elif 'q' in keys:
                core.quit()
            elif (akey=='l'): 
                expt.adjust_line_spacing( expt.line_spacing+0.05 )
                redraw=True
            elif (akey=='k'): 
                expt.adjust_line_spacing( expt.line_spacing-0.05 )
                redraw=True
                
            elif (akey=='comma'): 
                expt.height -= 1.0
            elif (akey=='period'): 
                expt.height += 1.0

            #elif (akey=='b'):
                #expt.win.clearBuffer()
                #expt.background.draw()
                #self.win.getMovieFrame(buffer="back")    
                #self.win.saveMovieFrames('screenshot.png')
                #quit=True

#            elif (akey=='left'):
#                expt.adjust_pos(-1,0)
#                redraw=True
#            elif akey=='right':
#                expt.adjust_pos( 1,0)
#                redraw=True

            # Manual control of "current line" 
            elif akey=='up':
                self.line_dec()
                extra_keys+=[akey] #let caller know what we did
            elif akey=='down' :
                self.line_inc()
                extra_keys+=[akey]#let caller know what we did
            elif akey=='left':
                expt.npage -= 1
                if expt.npage<=0:
                    expt.npage=0
                expt.which_line=0
                self.make_background()
                if not (tracker is None):
                    tracker.sendMessage("CLAB PAGE LEFT")
                if self.newpage_fixation_screen:
                    self.show_newpage_fixation()
                if not (tracker is None):
                    tracker.sendMessage("CLAB PAGE GO")                
                extra_keys+=[akey]#let caller know what we did
            elif akey=='right' or akey=='bracketright':
                expt.npage += 1
                if expt.npage>expt.maxpage:
                    if not (tracker is None):
                        tracker.sendMessage("CLAB PAGE RIGHT")
                    quit=True
                    break
                expt.which_line=0
                self.make_background()
                if not (tracker is None):
                    tracker.sendMessage("CLAB PAGE RIGHT")
                if self.newpage_fixation_screen:
                    self.show_newpage_fixation()
                if not (tracker is None):
                    tracker.sendMessage("CLAB PAGE GO")                
                extra_keys+=[akey]#let caller know what we did            

            elif (akey>='0') and (akey<='9'):
                # auto LF if on last word
                if self.stims_words[self.highlighted].eol:
                    if (self.line_inc()):
                        if not (tracker is None):
                            tracker.sendMessage("CLAB PAGE RIGHT")
                        quit=True
                extra_keys += [akey] # Also propogate to caller, so it can put it Eyetrace file
            else:
                #print("Don't understand key: %s"%akey)
                extra_keys += [akey]

        return quit,redraw,rerender,extra_keys

def spaceout(s):
    s_spaced=" ".join(s)
    return s_spaced

if __name__=="__main__":
    paragraph=238
    #sys.argv=[',','reading_word_data_S01_1_T1_000.txt'] # only do this to output a movie
    if len(sys.argv)>1:

        if len(sys.argv[1])<4: # Assume it's a number
            paragraph=int(sys.argv[1])
        else:
            # If file is passed in, use that for words, an no Gaussian.
            # Just save a .png and quit
            import pandas
            df = pandas.read_csv(sys.argv[1], delimiter='\t', header=None)
            paragraph=" ".join( list(df[0]) )

        save_and_quit=True
        apply_gaussian=False
        apply_highlight=False
        show_scotoma=False
        show_fixation=False
        king_devick_mode=False
        print (sys.argv[1])
    else:
        save_and_quit=False
        apply_gaussian=False
        apply_highlight=True
        show_fixation=True
        show_scotoma=True
        king_devick_mode=True

    expt=reading_expt(paragraph,max_lines=1) # 3 is a short one (Tesla)
    myWin = visual.Window( (1024,768), allowGUI=True, units='pix', screen=0 )
    
    myWin.setMouseVisible(True)
    mouse=event.Mouse(visible=True)
    mouse.setPos( [0,0] )

    expt.scotoma_radius=132 #Scotoma radius in pixels: match simulated
    expt.scotoma_prl=0 # PRL in degrees from fovea (0 is RVF, 90 is UVF, 270 is LVF etc..)
    expt.direction='left' #denotes the starting point for reading (right vs left testing) hence this can be either right or left
    expt.newpage_fixation_screen=(save_and_quit==False)
    
    expt.setup(myWin)
    expt.apply_gaussian=apply_gaussian
    expt.apply_highlight=apply_highlight
    expt.king_devick_mode=king_devick_mode
    expt.draw_setup() 
    
    scotoma=expt.make_mask(mask_radius=expt.scotoma_radius,num_lets=100)

    if False: #do_spaceout:
        for n,astim in enumerate(expt.stims_words):
            astim.text=spaceout(astim.text)

    expt.make_background()
    if expt.newpage_fixation_screen:
        expt.show_newpage_fixation()

    # After here, the experiment starts.
    quit=False

    while quit==False:
        keys=event.getKeys() 
        quit,redraw,rerender,extra_keys=expt.process_keys(keys)

        expt.eye_pos=mouse.getPos()
        expt.background.draw()
        expt.show_highlight()

        if show_scotoma:
            scotoma.pos=expt.eye_pos
            scotoma.draw()

        if show_fixation:
            expt.show_fixation_sword()
                
        if ('b' in extra_keys) or save_and_quit:
                expt.win.getMovieFrame(buffer="back")    
                expt.win.saveMovieFrames('screenshot.png')
                break

        #if ('1' in extra_keys):
            #if expt.stims_words[self.highlighted].pos[0],
            #print ("1") # test of auto LF
            
        myWin.flip()

    myWin.close()
