# -*- coding: utf-8 -*-
# Gaze contingent presentation based on psychopy demo script
# Nat Melnik
# C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\hw\sr_research\eyelink\eyetracker.py
# supported dev: C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\supported_config_settings.yaml
from psychopy import visual, core, monitors,event
from psychopy.sound import Sound
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
from psychopy import logging
logging.console.setLevel(logging.CRITICAL)
import os
import numpy
import numpy as np

import show_paragraph

thresh_pixels=9999
screen_resolution_vertical=768
screen_resolution_horizontal=1024
diagonal_pixel=np.sqrt(((screen_resolution_vertical)**2)+((screen_resolution_horizontal)**2))
diagonal_screensize_cm=44.5
pixel_per_cm=diagonal_pixel/diagonal_screensize_cm
distance_to_screen_cm= 42
scotoma_radius=10 #Scotoma radius in pixels
scotoma_radius_cm=scotoma_radius/pixel_per_cm
scotoma_radius_degrees= np.arctan(scotoma_radius_cm/distance_to_screen_cm) * 180/np.pi
fix_radius=10 # Fixation size (pixels from the center to the outside of the cross)
fix_lw=2

# Now set scotoma_prl so that show_paragraph works...
#pos_highlight=[0,-scotoma_radius*1.2] #IVF
#pos_scotoma=[-scotoma_radius*1+50,0] #LVF
#pos_scotoma=[ scotoma_radius*1+50,0] #RVF

try:
    os.remove('et_data.EDF')#nm: remove edf file from the previous trial... in ideal case should save directly with another name... have not figured out yet.
except OSError:
    pass

class ExperimentRuntime(ioHubExperimentRuntime):
    def run(self,*args):
        np.random.seed(self.rand_seed) #Set this to a random number if in case simulated needs to be run individually
        display=self.hub.devices.display
        kb=self.hub.devices.keyboard
        mouse=self.hub.devices.mouse
        tracker=self.hub.devices.tracker

        res=display.getPixelResolution() # Current pixel resolution of the Display to be used

        monsize =  (screen_resolution_horizontal, screen_resolution_vertical)
        mouse.setSystemCursorVisibility(False)
        clock = core.Clock()

        tracker.runSetupProcedure()
        
        window=visual.Window(monsize, units='pix', fullscr=False,allowGUI=False,color=[0,0.0,0], screen=display.getIndex())
        window.recordFrameIntervals=False
 
        self.hub.clearEvents('all')
        tracker.setRecordingState(False)
        
        max_lines=8
        expt=show_paragraph.reading_expt(self.paragraph_number, max_lines=max_lines)
        expt.setup(win=window)
        expt.draw_setup()

        # For recording the parameters of the text that is displayed on screen:
        word_file=open('Stimulus_Parameters_Words.txt','w')
        for stim1 in expt.stims_words:
            word_file.write(str(stim1.text)+'\t'+str(stim1.pos[0])+'\t'+str(stim1.pos[1])+'\n')
        word_file.close()
        
        #This sends the different messages regarding the experimental conditions to the eye tracker which is then used to 
        #write in the final EDF file. 
        tracker.sendMessage("CLAB Paragraph_number=%d max_lines=%d"%(self.paragraph_number,max_lines))
        tracker.sendMessage("CLAB subject=%s session_num=%d trial_num=%d"%(self.subject,self.session_num,self.trial_num))
        tracker.sendMessage("CLAB eye mode=%s"%self.eye_mode)
#        tracker.sendMessage("CLAB Random Seed=%s"%self.rand_seed)

        #The following set of lines set the different parameters based on the eye condition that is being run. The eye condition
        #is is set based on a protocol file that is created before running the experiment. This can also be manually set by changing the parameters
        #at the end of this program. 
        # Normal foveal mode:
        if self.eye_mode=="foveal":
            show_scotoma=True # Whether to show GC scotoma
            show_gc_fixation=False
            expt.apply_highlight=False
            expt.apply_gaussian=False
        elif self.eye_mode=="simulated only": #Scotoma condition without any guides presented
            show_scotoma=True
            show_gc_fixation=self.fixation_cross#Whether to GC fixation cross or not 
            expt.apply_highlight=False
            expt.apply_gaussian=False
        elif self.eye_mode=="simulated with guides": # Scotoma condition with guides except gaussian highlight
            show_scotoma=True # Whether to show GC scotoma
            show_gc_fixation=self.fixation_cross#Whether to GC fixation cross or not 
            expt.apply_highlight=True
            expt.apply_gaussian=False
        #This condition was used in the first experiment and it was observed that it was not too helpful to the subjects 
        #when performing the reading task
        #elif self.eye_mode=="simulated with all guides":# Scotoma condition with all guides shown
        #    show_scotoma=True # Whether to show GC scotoma
        #    show_gc_fixation=True
        #    expt.apply_highlight=True
        #    expt.apply_gaussian=True
        
        scotoma=expt.make_mask(scotoma_radius,num_lets=100) # New: scrambled letters
        expt.scotoma_radius=scotoma_radius 
        expt.make_background()
        
        expt.scotoma_prl=self.scotoma_prl
        
        expt.show_newpage_fixation()
        
        window.recordFrameIntervals=True
        
        clock.reset()
        self.hub.clearEvents('all')
        tracker.setRecordingState(True)
        #This denotes the beginning of the experiment and useful especially when one is parsing the data
        tracker.sendMessage("CLAB START EXPT")

        prev_pos=[0,0]
        #This is used to provide a audio feedback to the user to indicate that they have tried to make a foveating
        #saccade
        beep_feedback=core.CountdownTimer()
        beep_sound=Sound(value=1000,secs=0.25)
        foveation_cond=self.foveation_sensitivity
        #This is to ensure that the foveation detector is always turned off during foveal 
        if self.eye_mode=="foveal":
            foveation_cond="off"
        expt.eye_pos=prev_pos
        
        done=False
        while done==False:
            keys=event.getKeys()
            done,redraw,rerender,extra_keys=expt.process_keys(keys,tracker=tracker)
            
            if done:
                window.flip()
                break
                
            if len(extra_keys)>0:
                if 'g' in extra_keys:
                    show_scotoma=not (show_scotoma)
                    
                elif extra_keys[0] in ['num_0','num_1','num_2','num_3','num_4','num_5','num_6','num_7','num_8','num_9',
                        'up', 'down','enter','return']:
                    tracker.sendMessage("CLAB NUM %s"%extra_keys[0])
           
            gpos=tracker.getPosition()
            if type(gpos) in [tuple,list]:
                #The followoing line basically helps remove the jitter of the highlighted word; which basically occurs for when 
                #when the subject has their eyes positioned between two words which in turn causes the highlight to jump back and forth
                #In addition to that this prevents the jitter of simulated scotoma which occurs even when the subject is "fixating".
                #The value for comparison was initially chosen to be some arbitary value (5)
                if np.sqrt((prev_pos[0]-gpos[0])**2 + (prev_pos[1]-gpos[1])**2)>2.5:
                    expt.eye_pos=gpos
                #This identifies if the subject is attempted to make a foveating saccade in which case there is a beep sound that is played 
                #to indicate the same. Two levels of sensitivity are incorporated in order to identify the foveating saccades
                #NOTE: Please comment out the next 8 lines if simulated is to be run separately 
                if foveation_cond=="high":
                    if ((gpos[0]-prev_pos[0])>0) and ((gpos[1]-prev_pos[1])<-15) and (beep_feedback.getTime()<0):
                        beep_sound.play()
                        beep_feedback.reset(1)
                elif foveation_cond=="low":
                    if ((gpos[0]-prev_pos[0])>5) and ((gpos[1]-prev_pos[1])<-20) and (beep_feedback.getTime()<0):
                        beep_sound.play()
                        beep_feedback.reset(1)
                else:
                    pass
                #For CRT display we saw if the change in position was greater than 1 pixel(euclidian dist)
                if np.sqrt((prev_pos[0]-gpos[0])**2 + (prev_pos[1]-gpos[1])**2)>1:
                    scotoma.pos = (gpos[0],120) # Hard coded y position
                prev_pos=gpos

            #expt.background.draw() # The page of text of B/W text (background)
            #expt.show_highlight()  # Internally will check for condition
            if show_scotoma: 
                scotoma.draw()
            if show_gc_fixation:
                expt.show_fixation_sword() # internally will check for condition
                
            flip_time=window.flip()
        
            
        # After the trial loops exits:
        window.recordFrameIntervals=False
        tracker.setRecordingState(False)
        tracker.setConnectionState(False)
        
        # First find a new filename for the et_data and rename downloaded file
        # Do this for each trial
        unique_index=0
        while True:
            newname='reading_et_data_%s_%d_T%d_%03d.edf'%(self.subject,
                    self.session_num,self.trial_num,unique_index)
            word_file_name='reading_word_data_%s_%d_T%d_%03d.txt'%(self.subject,
                    self.session_num,self.trial_num,unique_index)
            try:
                os.stat(newname)
                
            except:
                break
                
            unique_index += 1
        os.rename('et_data.EDF',newname) # renames the EDF file
        os.rename('Stimulus_Parameters_Words.txt',word_file_name)#renames the word data file

        self.hub.quit()
        window.saveFrameIntervals()
        window.close()
        
        print ("\nEyelink EDF data file name: %s\n"%newname)
        print ("\nWord data file name: %s\n"%word_file_name)
        
if __name__=="__main__":
    runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
    runtime.eye_mode="foveal"
    runtime.fixation_cross=True
    runtime.paragraph_number=233
    runtime.subject="S01"
    runtime.rand_seed=3
    runtime.scotoma_prl= 270 #270# inferior visual field #0 #left visual field #180 right visual field
    runtime.foveation_sensitivity="low"
    runtime.session_num=1
    runtime.trial_num=1
    runtime.start()
    
    


