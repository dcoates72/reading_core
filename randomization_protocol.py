# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 14:15:44 2018

@author: krish
"""
import random
import pandas as pd
import numpy as np
import os
#Imports the passages from the csv file and is then converted to a numpy array
text_file=pd.read_csv("wikipedia_passages_comprehension.csv",delimiter='\t',header=None)
text_file_array=np.array(text_file)
len_word_lowlim=475
len_word_uplim=525
sorted_text_index=[]
#Iterates throuugh the paragraphs and gets the length of each passage and it passes through if it matches the set length criterion. 
for i in range(len(text_file_array[:,1])):
    if len(text_file_array[i,1])>len_word_uplim:
        pass
    elif len(text_file_array[i,1])<len_word_lowlim:
        pass
    else:
        sorted_text_index.append(i)
file_location=os.path.join(os.getcwd(),'Randomization_Protocol')
#os.makedirs(file_location)
os.chdir(file_location)
# This Program Randomizes the order of the experimental protocol that is to be followed in each session of the experiment
#The following dictionary is the key for the experimental conditions that are to be implemented
conditions=['foveal', 'simulated only','simulated with guides']
num_subjects=4
trial1=[]
num_para_session=4
num_para_total=34
for i in range(1,100):
    a=random.choice(conditions)
    if a not in trial1:
        trial1.append(a)
    if len(trial1)==num_para_session:
        break
    
trial2=trial1[::-1]
paragraph=[]
while len(paragraph)<(num_para_total):
    #for i in range(0,100):
        b=random.choice(sorted_text_index)
        if b not in paragraph:
            paragraph.append(b)
      

#Creates a random protocol for each subject and is asssigned a respective subject ID    
for i in range(1,num_subjects+1):
    subj_id='S%02d'%(i)
    random_1=open(str(subj_id)+'_01.txt',"w")
    random_2=open(str(subj_id)+'_02.txt',"w")
    subj_trial1=np.random.permutation(trial1)
    subj_trial2=subj_trial1[::-1]
    random_1.write(subj_id+'\n'+str(subj_trial1[0])+','+str(paragraph[0])+'\n'+str(subj_trial1[0])+','+str(paragraph[4])+'\n'+str(subj_trial1[1])+','+str(paragraph[1])+'\n'+str(subj_trial1[1])+','+str(paragraph[5])+'\n'+str(subj_trial1[2])+','+str(paragraph[2])+'\n'+str(subj_trial1[2])+','+str(paragraph[6])+'\n'+str(subj_trial1[3])+','+str(paragraph[3])+'\n'+str(subj_trial1[3])+','+str(paragraph[7])+'\n')
    random_2.write(subj_id+'\n'+str(subj_trial2[0])+','+str(paragraph[8])+'\n'+str(subj_trial2[0])+','+str(paragraph[12])+'\n'+str(subj_trial2[1])+','+str(paragraph[9])+'\n'+str(subj_trial2[1])+','+str(paragraph[13])+'\n'+str(subj_trial2[2])+','+str(paragraph[10])+'\n'+str(subj_trial2[2])+','+str(paragraph[14])+'\n'+str(subj_trial2[3])+','+str(paragraph[11])+'\n'+str(subj_trial2[3])+','+str(paragraph[15])+'\n')
    random_1.close()
    random_2.close()
    
#Creates the learning module using the number of training module/trials that is required by the experimenter which is entered into the variable trial_num   
num_training=5 #Each item in the list corresponds to the number of times we would want to perform the training module. 
training=open("learning_package.txt","w")
for i in range(num_training):
    training.write(str(conditions[0])+','+str(paragraph[16+i])+'\n')
for i in range(num_training):    
    training.write(str(conditions[1])+','+str(paragraph[16+num_training+i])+'\n')
for i in range(num_training):  
    training.write(str(conditions[2])+','+str(paragraph[16+2*(num_training)+i])+'\n')
training.close()

qna=open("answer_file.csv","w",encoding="utf-8")
temp=[]
for i in range(0,389):
    qna.write(str(i)+'\t'+str(text_file_array[i,2])+'\t'+str(text_file_array[i,3])+'\t\t\t\t'+str(text_file_array[i,4])+'\t'+str(text_file_array[i,5])+'\t\t\t\t'+str(text_file_array[i,6])+'\t'+str(text_file_array[i,7])+'\t\t\t\t'+'\n')
for i in range(403,461):
    qna.write(str(i)+'\t'+str(text_file_array[i,2])+'\t'+str(text_file_array[i,3])+'\t\t\t\t'+str(text_file_array[i,4])+'\t'+str(text_file_array[i,5])+'\t\t\t\t'+str(text_file_array[i,6])+'\t'+str(text_file_array[i,7])+'\t\t\t\t'+'\n')
qna.close()  

qna_filter=open("answer_file_filter.csv","w",encoding="utf-8")
temp_filter=[]
for i in paragraph:
    qna_filter.write(str(i)+'\t'+str(text_file_array[i,1])+'\t'+str(text_file_array[i,2])+'\t'+str(text_file_array[i,3])+'\t\t\t\t'+str(text_file_array[i,4])+'\t'+str(text_file_array[i,5])+'\t\t\t\t'+str(text_file_array[i,6])+'\t'+str(text_file_array[i,7])+'\t\t\t\t'+'\n')
    temp_filter.append(i)
qna_filter.close()

