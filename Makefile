#
#poster/%.eps: %.pdf
	#inkscape $< --export-eps=$@

all: size16.bmp size34.bmp size42.bmp size22.bmp

%.bmp: %.png
	convert  $< -colorspace gray -depth 8 bmp3:$@
#convert in.png -colorspace gray -depth 8 bmp2:out.bmp

%.bmp: screenshot.png
	convert  $< -colorspace gray -depth 8 bmp3:$@
