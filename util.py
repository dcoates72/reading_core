import numpy as np
#import params2 as params
#import params2 as params2
#import rfparams as rfparams
import sys
import uns
import os 

def buildtrialseq( vals, ntrials):
	return np.random.permutation( np.tile( vals, np.ceil( float(ntrials)/len(vals) ) ))[0:ntrials]

def unhist( data ):
	vals = np.unique( data )
	indices=[np.where( data==aval) for aval in vals]
	return vals, indices

def dumpvars( module, f=None ):
	#eval( "import %s"% module.__name__ ) #TODO: need to import the file above!? hack

	if f==None:
		f=sys.stdout

	vas=dir( module )
	for avar in vas:
		if avar[0] != "_":
			#f.write("#%s=%r\n"%(avar,eval("%s.%s"%(module.__name__,avar) ) ) )
			thevar=getattr(module,avar)
			if hasattr(thevar, '__iter__' ):
				theval="[%s]"%( ','.join(map(str,thevar)))
			else:
				theval="%s"%(thevar)
			f.write("#%s=%s\n"%(avar,theval) )

def deg2pix(deg):
	pixels_per_cm = params.screendim[1]/float(params.screen_height_cm)
	desired_cm = np.tan( deg * np.pi / 180.0 ) * params.distance_cm
	desired_pixels = desired_cm * pixels_per_cm
	return desired_pixels

def cm2deg(cm):
	degs = np.arctan( cm / params.distance_cm ) / np.pi * 180.0
	return degs

def cm2pix(cm):
	pixels_per_cm = params.screendim[1]/float(params.screen_height_cm)
	pix = cm*pixels_per_cm
	return pix

#minslope=0.005
#maxslope=0.5
def fixparms(p, B=100):
	#slope_recover = np.log10(p[1]/minslope) / np.log10(maxslope/minslope)
	slope_recover=slope2(p[1])
	#return p[0],0.5*np.log10(p[1]*B/5.0)-np.log10(minslope_div)/2.0
	return p[0]*B, slope_recover*B

# Convert slope in linear range (0,1) to 0.005,0.05 (log steps)
def slope2(sigma, minslope=0.005, maxslope=0.5):
	sigma = minslope * 10**(sigma/maxslope)
	return sigma

def PF(x,p, B=100):
	(B_0,B_1)=np.copy( p ) # careful: i've had bugs when aliasing the opt. params
	lapse=1./8
	#B_1 = 0.5 * np.log10( B_1 / 5.0 ) + 1.5 # do (Strasburger, #14) range conversion if necessary
	#B_1 = 0.5 * np.log10( B_1 / 5.0 ) + 1.5 # do (Strasburger, #14) range conversion if necessary
	B_1 = slope2( B_1 )
	pf=1./(1.+np.exp( (B_0-x)/B_1))
	return lapse+(1.-lapse)*pf

def estPF( p, c,ic ):
	#LL=np.sum( np.concatenate( (np.log(PF(c,p)), np.log( 1.0-PF(ic,p) ) ) ) )
	wrongs=np.log( 1.0-PF(ic,p))
	wrongs=1.0-PF(ic,p)
	wrongs[wrongs==0] = np.finfo('float').eps # fix to change anything that was PF=1
	wrongs=np.log( wrongs )
	LL=np.sum(np.log(PF(c,p))) + np.sum( wrongs )
	#print c
	#print ic
	#print -LL, p
	return -LL

def grids( c, ic, numgrid=21, doplot=False):
	P0 = np.linspace(0,1.0,numgrid)
	P1 = np.linspace(0,1.0,numgrid)
	#(X,Y)=
	LLs = np.array( [[estPF( (x,y),c,ic) for x in P0] for y in P1] )

	if doplot:
		plt.figure()
		plt.imshow( np.max(LLs)/LLs, interpolation='none', extent=(0,1,0,1) )
		plt.colorbar()

	return LLs

import matplotlib.pyplot as plt
def plotfit( p,c,ic, newfig=True, colr='b' ):
	if newfig: plt.figure()
	plt.plot( ic+np.random.normal(size=len(ic))/10000., np.zeros(len(ic))+np.random.normal(size=len(ic))/100, 'rx' )
		#plt.plot( cor+np.random.normal(size=len(cor))/50, x_all[cor], 'go' )
	plt.plot( c+np.random.normal(size=len(c))/10000., np.ones(len(c))+np.random.normal(size=len(c))/100, 'go' )
	plt.ylim(-0.1,1.1 )
	plt.xlim(0.0,1.0 )
	xr=np.linspace( np.min(ic), np.max(c) )
	plt.plot( xr, PF(xr, p), color=colr )

	# need "uns" unhist, which lets pass in values at which to evaluate, rather than doing unique
	all_uns = np.unique( np.concatenate( ( c, ic ) ) )
	num_c=np.array( uns.unhist( c, all_uns )[1], dtype='float' )
	num_ic=np.array( uns.unhist( ic, all_uns )[1], dtype='float' )

	n=num_c+num_ic
	p_hat = num_c/n
	conf95 = 1.96 * np.sqrt(p_hat*(1.-p_hat)/n)
	se_hat = np.sqrt(p_hat*(1.-p_hat)/n)

	plt.errorbar( all_uns, p_hat, yerr=se_hat, marker='o', ls='', color=colr )
	plt.grid( True )

import scipy.optimize
testC=np.array([0.5,0.75,1.0])
testIC=np.array([0.1,0.25,0.5])
def mlpf(c,ic,inits=(0.1,0.5)):
	return scipy.optimize.fmin_tnc( estPF, x0=inits, args=(c,ic), approx_grad=True, bounds=( (-1,100), (0.000001,1) ), disp=False ) 

# Find a unique filename by appending a number to the session
def get_unique_filename(filename_prefix):
	blockidx = 0
	gotunique = False
	while gotunique==False:
		outfilename = filename_prefix % blockidx
		try:
			if os.path.exists(outfilename):
				blockidx += 1
			else:
				gotunique = True
		except:
			try:
				os.mkdir('results')
				print ("Created new results directory.")
			except:
				print( "Fatal problem with results directory. Exiting." )
				sys.exit(-1)
	print outfilename
	return outfilename

def calibrate_timing(win, textstim, event, size=30, numloops=60, avg_range=(20,30), message_after="Ready. Hit any key."):
# Calibrate by seeing how long X redraws takes
	textstim.setHeight(size)
	msg = 'Calibrating...'
	textstim.setText( msg.upper() ) # sloan needs to be uppercase
	for i in np.arange(numloops):
		textstim.draw()
		win.flip()
	savetimes = win.frameIntervals
	fliprate = np.mean( savetimes[avg_range[0]:avg_range[1]] )
	textstim.setText( message_after.upper()  )
	textstim.draw()
	win.flip()
	event.waitKeys()
	return fliprate

def draw1(duration, stims, myWin, fliprate, ramp=False, mask=False, trial_cycles=1):
	# Display for a certain number of "flips," as close to duration as technically possible
	myWin.blendMode='avg'
	if (duration > 0):
		numflips = int( duration / fliprate )
		if numflips<1:
			numflips=1
		for angle in np.linspace( 0, np.pi*(trial_cycles/1.0), numflips ):
			contr = -0.5 + -0.5 * abs(np.sin( angle) )
			[stim.draw() for stim in stims]
			myWin.blendMode='avg'
			myWin.flip()
	return numflips * fliprate

# IF when=0 donothing, just return
def draw_wait( win, core, what, duration=0 ):
	if duration>0:
		[athing.draw() for athing in what]
		win.blendMode='avg'
		win.flip()
		core.wait(duration)
