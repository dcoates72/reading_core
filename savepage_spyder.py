# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 14:42:01 2019

@author: CLAB
"""

import psychopy.visual as visual 
import pandas as pd
import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()

import show_paragraph
repeat='True'
while repeat=='True':

    # sys.argv=[',','reading_word_data_S01_1_T1_000.txt'] # only do this to output a movie
    data_directory=filedialog.askdirectory(title='Please select data directory')

    myWin = visual.Window( (1024,768), allowGUI=True, units='pix', screen=0 )
    myWin.setMouseVisible(True)

    for dirName,subdirist,filelist in os.walk(data_directory):     
        for filename in filelist: # list of files 
            if "reading_word_data" in filename: 
                df = pd.read_csv(filename, delimiter='\t', header=None)
                paragraph=" ".join( list(df[0]) )
                save_and_quit=True
                apply_gaussian=False
                apply_highlight=False
                show_scotoma=False
                show_fixation=False
                
                expt=show_paragraph.reading_expt(paragraph,max_lines=8) 
                
                expt.setup(myWin)
                expt.apply_gaussian=apply_gaussian
                expt.apply_highlight=apply_highlight
                expt.draw_setup() 
                expt.make_background()
                expt.background.draw()
                expt.win.getMovieFrame(buffer="back")    
                expt.win.saveMovieFrames(str(filename)+'.png')
                  
                expt.win.flip() # to clear the old image

    # end of for loop
    myWin.close()
