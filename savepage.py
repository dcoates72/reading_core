import psychopy.visual as visual
import psychopy.gui 
import pandas as pd
import os
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()

import show_paragraph

data_directory=filedialog.askdirectory(title='Please select data directory')
#repeat='True'
#while repeat=='True':
for dirName,subdirlist,filelist in os.walk(data_directory):
    #os.chdir('C:/Users/C-LAB/projects/reading/Results/Experiment 2 King Devick Mode/All Subjects/Raw Data')
    # sys.argv=[',','reading_word_data_S01_1_T1_000.txt'] # only do this to output a movie
    #data_directory=psychopy.gui.fileOpenDlg(tryFilePath='', tryFileName='', prompt='Select file to open', allowed=None)
    #data_directory=["reading_word_data_S01_1_T1_000.txt"]
    myWin = visual.Window( (1920,1080), fullscreen=True, allowGUI=True, units='pix', screen=0 )
    
    myWin.setMouseVisible(True)
    for filename in os.listdir(dirName):    
        if "reading_word_data" in filename and 'page' not in filename: 
            df = pd.read_csv(os.path.join(dirName,filename), delimiter='\t', header=None)
            paragraph=" ".join( list(df[0]) )
            save_and_quit=True

            expt=show_paragraph.reading_expt(paragraph,max_lines=1) 
            
            expt.setup(myWin)
            expt.apply_gaussian=False
            expt.apply_highlight=False
            expt.show_scotoma=False
            expt.show_fixation=False
            expt.newpage_fixation_screen=False
            #expt.scotoma_prl=270
            #expt.direction='right'

            expt.draw_setup() 
            
            for page in range(8):
                #create data frame

                # Save one line                    
                expt.make_background()
                expt.background.draw()
                expt.win.getMovieFrame(buffer="back")   
                image_filename=os.path.join(dirName,filename[0:-8])
                expt.win.saveMovieFrames("%s_page%d.png"%(image_filename,page+1) )
                expt.win.flip() # to clear the old image
    
                # Go to next line
                expt.process_keys(['right'])
                word_data=pd.DataFrame({'Word':[],'page_num':[]})
                for aword in expt.stims_words:
                    if aword.npage==page:
#                        print aword.text, " ",page+1
                        word_data=word_data.append({'Word':aword.text,'page_num':page+1},ignore_index=True)
                
                word_data.to_csv(os.path.join(dirName,'%s_page%d.txt'%(filename[0:-8],page+1)),sep='\t',index=False)
    # end of for loop
    myWin.close()
            
#    repeat="False" # Remove this to repeat forever